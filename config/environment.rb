# Load the rails application
require File.expand_path('../application', __FILE__)

LIVE_PERSONS_EMAIL       = 'help@example.com'

FACEBOOK_APP_ID          = "170924039661923"
FACEBOOK_APP_SECRET      = "dec03158c5decce59c72ec18d476a277"
FACEBOOK_APP_PERMISSIONS = "email,offline_access,publish_stream"
FACEBOOK_CANVAS_URL      = 'http://apps.facebook.com/dsbetaone'

DEFAULT_FB_SHARE_IMAGE   = "http://localhost:3000/images/missing.png"
DEFAULT_FB_POST_NAME     = "Dreamscape"
DEFAULT_PAGE_TITLE       = "Dreamscape"
DEFAULT_PAGE_DESCRIPTION = "Dreamscape"

# Rails.application.config.middleware.use OmniAuth::Builder do
#    provider :facebook, FACEBOOK_APP_ID, FACEBOOK_APP_SECRET,
#    :iframe => true,
#    :Railsscope => "publish_stream,email,user_education_history"
# end
#
#OmniAuth.config.full_host = FACEBOOK_CANVAS_URL

# Initialize the rails application
Dreamscape::Application.initialize!
