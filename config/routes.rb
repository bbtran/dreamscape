Dreamscape::Application.routes.draw do

  resources :tasks, :only => [:edit, :update, :create, :destroy]
  resources :dreams, :only => [:edit, :update, :create, :destroy]
  resources :collaborations, :only => ['create', 'destroy']
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  get "pages/index"
  post "pages/index"
  root :to => 'pages#index'
end
