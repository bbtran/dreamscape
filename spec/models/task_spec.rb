require 'spec_helper'

describe Task do
  before(:each) do
    @user = Factory(:user, :email => Factory.next(:email))
    @dream = Factory(:dream, :user => @user)
    @attr = {
        :title => "Finish lab 4 for CMPS 160",
        :details => "Go to lucidbard.com/cs160 for more details",
        :completed => false,
        :user_id => @dream.user_id
    }
  end

  it "should create a new instance given valid attributes" do
    @dream.tasks.create!(@attr)
  end

  describe "user associations" do
    before(:each) do
      @task = @dream.tasks.create(@attr)
    end

    it "should have a user attribute" do
      @task.should respond_to(:user)
    end

    it "should have the right associated user" do
      @task.user_id.should == @user.id
      @task.user.should == @user
    end
  end

  describe "dream associations" do
    before(:each) do
      @task = @dream.tasks.create(@attr)
    end

    it "should have a dream attribute" do
      @task.should respond_to(:dream)
    end

    it "should have the right associated dream" do
      @task.dream_id.should == @dream.id
      @task.dream.should == @dream
    end
  end

  describe "assignment associations" do
    before(:each) do
      @task = @dream.tasks.create(@attr)
    end

    it "should have an assignments attribute" do
      @task.should respond_to(:assignments)
    end

    it "should have an assignees attribute" do
      @task.should respond_to(:assignees)
    end
  end

  describe "validations" do
    it "should require a user id" do
      Task.new(@attr.merge(:user_id => nil)).should_not be_valid
    end

    it "should require a title" do
      no_title_task = Task.new(@attr.merge(:title => ""))
      no_title_task.should_not be_valid
    end
  end
end
