require 'spec_helper'

describe UserToken do
  before(:each) do
    @user = Factory(:user, :email => Factory.next(:email))
    @attr = {
      :provider => 'facebook',
      :uid => '123',
      :token => 'aadfadsfasdf',
      :secret => 'asdfsewrwteitoi'
    }
  end
  
  it "should create a new instance given valid attributes" do
    @user.user_tokens.create!(@attr)
  end

  describe "validations" do
    it "should require a user id" do
      UserToken.new(@attr).should_not be_valid
    end

    it "should require a provider" do
      no_provider_ut = UserToken.new(@attr.merge(:provider => ""))
      no_provider_ut.should_not be_valid
    end

    it "should require a uid" do
      no_uid_ut = UserToken.new(@attr.merge(:uid => ""))
      no_uid_ut.should_not be_valid
    end

    it "should require a token" do
      no_token_ut = UserToken.new(@attr.merge(:token => ""))
      no_token_ut.should_not be_valid
    end
  end
end
