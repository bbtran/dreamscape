require 'spec_helper'

describe Collaboration do
  before(:each) do
    @user = Factory(:user, :email => Factory.next(:email))
    @dream = Factory(:dream, :title => "Graduate UC Santa Cruz",
      :details => "Work hard in school and get nutrition")
    @col = Factory(:collaboration, :dream => @dream, :collaborator => @user)
  end

  describe "validations" do
    before(:each) do
      @attr = { :collaborator_id => '' }
    end

    it "should require a dream id" do
      Collaboration.new(@attr).should_not be_valid
    end

    it "should require a collaborator id" do
      no_collab = Collaboration.new(@attr.merge(:dream_id => @dream.id))
      no_collab.should_not be_valid
    end
  end

  describe "collaborator associations" do
    it "should have a collaborator attribute" do
      @col.should respond_to(:collaborator)
    end

    it "should have right associated collaborator" do
      @col.collaborator_id.should == @user.id
      @col.collaborator.should == @user
    end
  end

  describe "dream associations" do
    it "should have a dream attribute" do
      @col.should respond_to(:dream)
    end

    it "should have right associated dream" do
      @col.dream_id.should == @dream.id
      @col.dream.should == @dream
    end
  end
end
