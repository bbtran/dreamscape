require 'spec_helper'

describe Dream do
  before(:each) do
    @user = Factory(:user, :email => Factory.next(:email))
    @attr = {
        :title => "Graduate UC Santa Cruz",
        :details => "Work hard in school and get nutrition"
    }
  end

  it "should create a new instance given valid attributes" do
    @user.dreams.create!(@attr)
  end

  describe "validations" do
    it "should require a user id" do
      Dream.new(@attr).should_not be_valid
    end

    it "should require a title" do
      no_title_dream = Dream.new(@attr.merge(:title => ""))
      no_title_dream.should_not be_valid
    end
  end

  describe "user associations" do
    before(:each) do
      @dream = @user.dreams.create(@attr)
    end

    it "should have a user attribute" do
      @dream.should respond_to(:user)
    end

    it "should have the right associated user" do
      @dream.user_id.should == @user.id
      @dream.user.should == @user
    end
  end

  describe "task associations" do
    before(:each) do
      @dream = Factory(:dream, :title => "Graduate UC Santa Cruz", :user => @user)
      @t1 = Factory(:task, :user => @user, :dream => @dream, :created_at => 1.day.ago)
      @t2 = Factory(:task, :user => @user, :dream => @dream, :created_at => 1.hour.ago)
    end

    it "should have tasks attribute" do
      @dream.should respond_to(:tasks)
    end

    it "should destroy associated tasks" do
      @dream.destroy
      [@t1, @t2].each do |t|
        lambda do
          Task.find(t)
        end.should raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe "collaboration associations" do
    before(:each) do
      @dream = Factory(:dream, :title => "Graduate UC Santa Cruz", :user => @user)
    end
    
    it "should have collaborations attribute" do
      @dream.should respond_to(:collaborations)
    end

    it "should have collaborators attribute" do
      @dream.should respond_to(:collaborators)
    end
  end
end
