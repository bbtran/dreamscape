require 'spec_helper'

describe Friendship do
  before(:each) do
    @user = Factory(:user, :name => Faker::Name.name, :email => Factory.next(:email))
    @friend = Factory(:user, :name => Faker::Name.name, :email => Factory.next(:email))
    @attr = { :friend_id => @friend.id }
  end

  it "should create a new instance with valid attributes" do
    @user.friendships.create!(@attr)
  end

  describe "add_friend methods" do
    before(:each) do
      @friendship = @user.friendships.create!(@attr)
    end

    it "should have a user attribute" do
      @friendship.should respond_to(:user)
    end

    it "should have the right user" do
      @friendship.user.should == @user
    end

    it "should have a friend attribute" do
      @friendship.should respond_to(:friend)
    end

    it "should have the right friend" do
      @friendship.friend.should == @friend
    end
  end

  describe "validations" do
    it "should require a user id" do
      Friendship.new(@attr).should_not be_valid
    end

    it "should require a friend id" do
      @user.friendships.build.should_not be_valid
    end
  end
end
