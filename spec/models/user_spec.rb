require 'spec_helper'

describe User do
  before do
    @user = Factory(:user)
  end

  describe "user tokens associations" do
    before(:each) do
      @ut1 = Factory(:user_token, :user => @user, :provider => 'facebook', :created_at => 1.day.ago)
    end

    it "should have a user_tokens attribute" do
      @user.should respond_to(:user_tokens)
    end

    it "should destroy associated user_tokens" do
      @user.destroy
      [@ut1].each do |ut|
        lambda do
          UserToken.find(ut)
        end.should raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe "dream associations" do
    before(:each) do
      @d1 = Factory(:dream, :user => @user, :created_at => 1.day.ago)
      @d2 = Factory(:dream, :user => @user, :created_at => 1.hour.ago)
    end

    it "should have a dreams attribute" do
      @user.should respond_to(:dreams)
    end

    it "should destroy associated dreams" do
      @user.destroy
      [@d1, @d2].each do |d|
        lambda do
          Dream.find(d)
        end.should raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe "task associations" do
    before(:each) do
      @dream = Factory(:dream, :user => @user)
      @t1 = Factory(:task, :user => @user, :dream => @dream, :created_at => 1.day.ago)
      @t2 = Factory(:task, :user => @user, :dream => @dream, :created_at => 1.hour.ago)
    end

    it "should have a tasks attribute" do
      @user.should respond_to(:tasks)
    end

    it "should destroy associated tasks" do
      @user.destroy
      [@t1, @t2].each do |t|
        lambda do
          Task.find(t)
        end.should raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe "friendship associations" do
    before(:each) do
      @friend1 =  Factory(:user, :name => Faker::Name.name, :email => Faker::Internet.email)
      @friend2 =  Factory(:user, :name => Faker::Name.name, :email => Faker::Internet.email)
      @fs1 = Factory(:friendship, :user => @user, :friend => @friend1)
      @fs1 = Factory(:friendship, :user => @user, :friend => @friend2)
    end

    it "should have a friendships attribute" do
      @user.should respond_to(:friendships)
    end

    it "should destroy associated friendships" do
      @user.destroy
      [@fs1, @fs2].each do |fs|
        lambda do
          Friendship.find(fs)
        end.should raise_error(ActiveRecord::RecordNotFound)
      end
    end

    it "should have a friends attribute" do
      @user.should respond_to(:friends)
    end

    it "should have right friends" do
      friends = [@friend1, @friend2].sort { |a, b| a.name <=> b.name }
      @user.friends.should == friends
    end

    it "should not destroy friends on destroying associated friendships" do
      @user.destroy
      User.find(@friend1).should == @friend1
      User.find(@friend2).should == @friend2
    end
  end

  describe "assignment associations" do
    before(:each) do
      @friend1 =  Factory(:user, :name => Faker::Name.name, :email => Faker::Internet.email)
      @friend2 =  Factory(:user, :name => Faker::Name.name, :email => Faker::Internet.email)
      @fs1 = Factory(:friendship, :user => @user, :friend => @friend1)
      @fs1 = Factory(:friendship, :user => @user, :friend => @friend2)
      @dream = Factory(:dream, :user => @user)
      @task1 = Factory(:task, :user => @user)
      @task2 = Factory(:task, :user => @user)
      @asg1 = Factory(:assignment, :assignee => @user, :task => @task1)
      @asg2 = Factory(:assignment, :assignee => @user, :task => @task2)
    end

    it "should have a assignments attribute" do
      @user.should respond_to(:assignments)
    end

    it "should destroy associated assignments" do
      @user.destroy
      [@asg1, @asg2].each do |asg|
        lambda do
          Assignment.find(asg)
        end.should raise_error(ActiveRecord::RecordNotFound)
      end
    end

    it "should have an assigned_tasks attribute" do
      @user.should respond_to(:assigned_tasks)
    end

    it "should have right assigned_tasks" do
      assigned_tasks = [@task1, @task2]
      @user.assigned_tasks.should == assigned_tasks
    end

    it "should not destroy friends on destroying associated friendships" do
      @user.destroy
      User.find(@friend1).should == @friend1
      User.find(@friend2).should == @friend2
    end
  end

  describe "collaborations associations" do
    before(:each) do
      @friend1 =  Factory(:user, :name => Faker::Name.name, :email => Faker::Internet.email)
      @friend2 =  Factory(:user, :name => Faker::Name.name, :email => Faker::Internet.email)
      @fs1 = Factory(:friendship, :user => @user, :friend => @friend1)
      @fs1 = Factory(:friendship, :user => @user, :friend => @friend2)
      @dream = Factory(:dream, :user => @user)
      @c1 = Factory(:collaboration, :dream => @dream, :collaborator => @friend1)
      @c2 = Factory(:collaboration, :dream => @dream, :collaborator => @friend2)
    end

    it "should have a collaborations attribute" do
      @user.should respond_to(:collaborations)
    end

    it "should destroy associated collaborations" do
      @user.destroy
      [@c1, @c2].each do |c|
        lambda do
          Collaboration.find(c)
        end.should raise_error(ActiveRecord::RecordNotFound)
      end
    end

    it "should not destroy collaborators on destroying associated collaborations" do
      @user.destroy
      User.find(@friend1).should == @friend1
      User.find(@friend2).should == @friend2
    end
  end

  describe "#password_required?" do
    describe "new users" do
      before(:each) do
        @user = User.new
      end

      it "requires a password when no user_token present" do
        @user.should be_password_required
      end

      it "should not require password when they have an auth token" do
        @user.apply_omniauth(omniauth_facebook)
        @user.should_not be_password_required
      end
    end

    describe "existing users" do
      before(:each) do
        @user.save!
        @user = User.find(@user.id)
      end

      it "requires a password when password is present" do
        @user.password = 'mistyped'
        @user.should be_password_required
      end

      it "requires a correct password when password_comfirmation is present" do
        @user.password_confirmation = 'xx'
        @user.should be_password_required
      end

      it "should not require password no password or confirmation is present" do
        @user.reload.should_not be_password_required
      end
    end
  end

  describe "validations" do
    describe "for omniauthed users" do
      it "should be valid with a token" do
        @user = User.new(:email => Faker::Internet.email,
                         :password => 'cool123', :password_confirmation => 'cool123')
        @user.save
        @user.apply_omniauth(omniauth_facebook)
        @user.should be_valid
      end
    end

    describe "for new users" do

      describe "for on-site registered users" do
        it "should require password & match confirmation" do
          @user.password = nil
          @user.password_confirmation = nil

          @user.password = "cool"
          @user.password_confirmation = nil

          @user.should_not be_valid

          @user.password = "cool"
          @user.password_confirmation = "hot"

          @user.should_not be_valid
        end

        it "should require email" do
          @user.email = nil
          @user.should_not be_valid
        end
      end
    end
  end

  describe "#apply_omniauth" do
    describe "facebook" do
      it "should create provider token and add credentials" do
        @user.user_tokens.should be_empty

        @user.apply_omniauth(omniauth_facebook)

        token = @user.user_tokens.first
        token.token.should_not be_nil
      end
    end

    it "should assign photo is image url is present and photo is empty" do
      @user.user_tokens.should be_empty
      @omni = omniauth_facebook
      @omni['info']['image'] = nil
      @user.apply_omniauth(@omni)
      @user.photo.should_not be_exists
    end


  end

  describe "#populate_from_twitter" do

    it "should populate the user's name" do
      @user.name = nil

      @user.apply_omniauth(omniauth_facebook)

      @user.name.should_not be_blank
      @user.name.should == omniauth_facebook['info']['name']
    end

    describe "when name is already set" do
      it "should not override an existing name" do
        @user.name = "Bob"
        @user.apply_omniauth(omniauth_facebook)
        @user.name.should == 'Bob'
      end
    end

  end

  describe "#populate_from_facebook" do
    before(:each) do
      @user = Factory(:user, :name => nil, :email => Faker::Internet.email)
    end

    it "should populate the user's name" do

      @user.apply_omniauth(omniauth_facebook)

      @user.name.should_not be_blank
      @user.name.should == omniauth_facebook['info']['name']
    end

    it "should populate the user's email" do
      @user.email = ''
      @user.apply_omniauth(omniauth_facebook)
      @user.email.should_not be_blank
      @user.email.should == omniauth_facebook['info']['email']
    end

    describe "when name is already set" do
      before(:each) do
        @user = Factory(:user, :name => 'Bob', :email => 'bob@example.com')
        @user.name = 'Bob'
        @user.apply_omniauth(omniauth_facebook)
      end

      it "should not override an existing name" do
        @user.name.should == 'Bob'
      end

      it "should not override an existing email" do
        @user.email.should == 'bob@example.com'
      end
    end

  end
end
