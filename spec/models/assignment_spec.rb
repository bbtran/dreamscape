require 'spec_helper'

describe Assignment do
  before(:each) do
    @user = Factory(:user, :email => Factory.next(:email))
    @dream = Factory(:dream, :title => "Graduate UC Santa Cruz",
      :details => "Work hard in school and get nutrition")
    @task = Factory(:task, :title => "Finish lab 5")
    @asg = Factory(:assignment, :assignee => @user, :task => @task)
  end

  describe "validations" do
    before(:each) do
      @attr = { :task_id => '' }
    end

    it "should require a task id" do
      Assignment.new(@attr).should_not be_valid
    end

    it "should require a collaborator id" do
      no_asg_assignee_id = Collaboration.new(@attr.merge(:task_id => @task.id))
      no_asg_assignee_id.should_not be_valid
    end
  end

  describe "assignee associations" do
    it "should have a assignee attribute" do
      @asg.should respond_to(:assignee)
    end

    it "should have right associated assignee" do
      @asg.assignee_id.should == @user.id
      @asg.assignee.should == @user
    end
  end

  describe "task associations" do
    it "should have a task attribute" do
      @asg.should respond_to(:task)
    end

    it "should have right associated task" do
      @asg.task_id.should == @task.id
      @asg.task.should == @task
    end
  end
end
