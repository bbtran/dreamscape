# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_token do
    user_id 1
    provider "facebook"
    uid "123456"
    token "MyString"
    secret "MyString"
  end
end
