FactoryGirl.define do
  factory :user do
    name                  "Bob Momsey"
    email                 "bob@dreamscape.com"
    password              "foobar"
    password_confirmation "foobar"
  end
end