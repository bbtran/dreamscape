# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :dream do
    user_id 1
    title "MyString"
    details "MyString"
  end
end
