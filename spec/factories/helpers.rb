Factory.sequence(:email) do |n|
  "person-#{n}@dreamscape.com"
end