require 'spec_helper'

describe "Sessions" do
  it "should login to facebook and redirect to root" do
    get root_path

    response.should redirect_to controller.user_omniauth_authorize_path(:facebook)
  end
end
