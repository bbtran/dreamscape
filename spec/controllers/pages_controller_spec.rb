require 'spec_helper'

describe PagesController do
  render_views

  before(:each) do
    request.env["devise.mapping"] = Devise.mappings[:user]
    request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:facebook]
  end

  describe "GET 'index'" do
    describe "when not signed in" do
      it "should redirect to facebook login" do
        get :index
        response.should redirect_to controller.user_omniauth_authorize_path(:facebook)
      end
    end

    describe "when signed in on facebook" do
      before(:each) do
        # Signing in user through facebook
        @user = Factory(:user)
        @user.apply_omniauth(omniauth_facebook)
        @user.save!
        sign_in(:user, @user)
      end
      
      it "should be successful" do
        get :index
        response.should be_success
      end
    end
  end
end
