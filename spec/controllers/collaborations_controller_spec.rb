require 'spec_helper'

describe CollaborationsController do
  render_views

  before(:each) do
    request.env["devise.mapping"] = Devise.mappings[:user]
    request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:facebook]
  end

  it "should deny access to 'create'" do
      post :create
      response.should redirect_to controller.user_omniauth_authorize_path(:facebook)
    end

  it "should deny access to 'destroy'" do
    delete :destroy, :id => 1
    response.should redirect_to controller.user_omniauth_authorize_path(:facebook)
  end
end
