#describe Users::OmniauthCallbacksController do
#
#  before(:each) do
#    request.env["devise.mapping"] = Devise.mappings[:user]
#  end
#
#  it "should have omniauth_data attribute" do
#    controller.should respond_to(:omniauth_data)
#  end
#
#  describe "/auth/:action/callback" do
#    describe "when user is logging in with a previously authed account" do
#      before(:each) do
#        @user = Factory(:user, :name => "", :email => 'existing@example.com')
#        @facebook_data = omniauth_facebook
#        @user.apply_omniauth(@facebook_data)
#        @user.save!
#
#        set_omniauth_credentials :facebook, @facebook_data
#
#        @request.env["omniauth.auth"] = @facebook_data
#      end
#
#      it "should set omniauth_data" do
#        get :facebook
#        controller.omniauth_data.should == @facebook_data
#      end
#
#      it "should be 1 user" do
#        User.all.count.should == 1
#      end
#
#      it "should lookup the correct user" do
#        get :facebook
#        User.all.count.should == 1
#        controller.current_user.should == @user
#      end
#
#      it "should not create new provider token" do
#        get :facebook
#        @user.reload
#        @user.user_tokens.size.should == 1
#      end
#
#      it "should redirect :back if :location set" do
#        session["user_return_to"] = "/?remember_the_alamo"
#        get :facebook
#        response.should redirect_to("/?remember_the_alamo")
#      end
#
#      it "should redirect to / if :back not set" do
#        get :facebook
#        response.should redirect_to('/')
#      end
#    end
#
#    describe "when user is already logged in and adding a provider account associated with another user" do
#      it "should tell the user to email for help" do
#        @other_acct = Factory(:user, :name => "", :email => 'existing@example.com')
#        @facebook_data = omniauth_facebook
#        @other_acct.apply_omniauth(@facebook_data)
#        @other_acct.save!
#
#        @user = Factory(:user, :name => "")
#        sign_in :user, @user
#
#        set_omniauth_credentials :facebook, @facebook_data
#
#        get :facebook
#        flash[:alert].should_not be_nil
#        flash[:alert].should =~ /help/
#      end
#    end
#
#    describe "when user is new and signing up/in" do
#      before(:each) do
#        @facebook_data = omniauth_facebook
#        set_omniauth_credentials :facebook, @facebook_data
#      end
#
#      it "should create user" do
#        get :facebook
#        @user = controller.current_user
#
#        @user.should_not be_nil
#        @user.should_not be_new_record
#      end
#
#      it "should create provider token" do
#        get :facebook
#        @user = controller.current_user
#
#        @user.user_tokens.facebook.token.should == @facebook_data["credentials"]["token"]
#        @user.user_tokens.facebook.secret.should == @facebook_data["credentials"]["secret"]
#      end
#
#      it "should redirect :back if :location set" do
#        session["user_return_to"] = "/?remember_the_alamo"
#        get :facebook
#        response.should redirect_to("/?remember_the_alamo")
#      end
#
#      it "should redirect to / if :back not set" do
#        get :facebook
#        response.should redirect_to('/')
#      end
#    end
#  end
#end