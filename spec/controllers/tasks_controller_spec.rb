require 'spec_helper'

describe TasksController do
  render_views

  before(:each) do
    request.env["devise.mapping"] = Devise.mappings[:user]
    request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:facebook]
  end
  
  describe "access control" do
    it "should deny access to 'create'" do
      post :create
      response.should redirect_to controller.user_omniauth_authorize_path(:facebook)
    end

    it "should deny access to 'edit'" do
      get :edit, :id => 1
      response.should redirect_to controller.user_omniauth_authorize_path(:facebook)
    end

    it "should deny access to 'update'" do
      @user = Factory(:user)
      @dream = Factory(:dream, :user => @user)
      @task = Factory(:task, :user => @user, :dream => @dream, :title => "do something")
      @attr = { :dream_id => @dream, :title => "some title" }

      put :update, :id => @task, :task => @attr
      response.should redirect_to controller.user_omniauth_authorize_path(:facebook)
    end

    it "should deny access to 'destroy'" do
      delete :destroy, :id => 1
      response.should redirect_to controller.user_omniauth_authorize_path(:facebook)
    end
  end
end
