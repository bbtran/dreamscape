class Assignment < ActiveRecord::Base
  attr_accessible :assignee_id

  belongs_to :assignee, :class_name => "User"
  belongs_to :task

  validates :assignee_id, :presence => true
  validates :task_id, :presence => true
end
