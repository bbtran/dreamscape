class User < ActiveRecord::Base
  include OmniAuthPopulator

  has_many :tasks, :dependent => :destroy
  has_many :dreams, :include => :tasks, :dependent => :destroy
  has_many :user_tokens, :dependent => :destroy do
    def facebook
      target.detect{|t| t.provider == 'facebook'}
    end

    def twitter
      target.detect{|t| t.provider == 'twitter'}
    end
  end
  
  has_many :friendships, :foreign_key => "user_id", :dependent => :destroy
  has_many :reverse_friendships, :foreign_key => "user_id", :class_name => "Friendship", :dependent => :destroy
  has_many :friends, :through => :friendships, :source => :friend, :order => 'users.name', :include => :dreams
  has_many :assignments, :foreign_key => "assignee_id", :dependent => :destroy
  has_many :assigned_tasks, :through => :assignments, :source => :task
  has_many :collaborations, :foreign_key => "collaborator_id", :dependent => :destroy

  has_attached_file :photo,
                    :styles => {
                            :mini => "40x40#",
                            :thumb => "80x80#",
                            :small => "100x100#",
                            :big => "150x150#"
                    },
                    :default_url => "/images/user_photos/missing_:style.png"
  
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :omniauthable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :facebook_friends
  
  def self.find_for_facebook_oauth(access_token, signed_in_resource=nil)
    data = access_token['extra']['raw_info']
    if user = User.find_by_email(data["email"])
      user
    else # Create a user with a stub password.
      User.new(:email => data["email"], :password => Devise.friendly_token[0,20])
    end
  end

  def apply_omniauth(omniauth)
    self.omniauth = omniauth
    user_tokens.build(:provider => omniauth['provider'], :uid => omniauth['uid'], :token => omniauth['credentials']['token'])

    #populate_photo_from_url(omniauth['info']['image']) unless photo.exists? || omniauth['info']['image'].blank?
  end

  def populate_from_twitter(omni)
    self.name = omni['info']['name'] if self.name.blank?
  end

  def populate_from_google_apps(omni)
    self.name = omni['info']['name'] if self.name.blank?
  end

  def populate_from_facebook(omni)
    self.name = omni['info']['name'] if self.name.blank?
    self.email = omni['info']['email'] if self.email.blank?
  end

#allows for account creation from twitter & fb
#allows saves w/o password
  def password_required?
    (!persisted? && user_tokens.empty?) || password.present? || password_confirmation.present?
  end

#allows for account creation from twitter
  def email_required?
    user_tokens.empty?
  end

  def remember_me
    super.nil? ? false : true
  end

  def add_friend!(friend)
    friendships.create!(:user_id => self.id, :friend_id => friend.id)
  end
  
  def add_friends!(friends)
    ActiveRecord::Base.transaction do
      for friend in friends
        friendships.create!(:user_id => self.id, :friend_id => friend.id)
      end
    end
  end
  
  private

  def populate_photo_from_url(image_url)
    require 'open-uri'
    io = open(URI.parse(image_url))

    def io.original_filename;
      base_uri.path.split('/').last;
    end

    self.photo = io.original_filename.blank? ? nil : io
    #todo for now throw, not sire the error cases
  end

  def truncated_message_with_url(message="", url="", length=140)
    if message.size + url.size > 140
      share = message[0..(136-url.size)] + "..." + url
    else
      share = message + " " + url
    end
    share
  end
end
