class Task < ActiveRecord::Base
  belongs_to :user
  belongs_to :dream

  has_many :assignments, :dependent => :destroy
  has_many :assignees, :through => :assignments, :source => :assignee

  validates :user_id, :presence => true
  validates :title,   :presence => true
end
