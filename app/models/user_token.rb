class UserToken < ActiveRecord::Base
  belongs_to :user

  validates :user_id, :presence => true
  validates :provider, :presence => true
  validates :uid, :presence => true

  def populate_from_twitter(omni)
    self.secret = omni['credentials']['secret']
    self.token  = omni['credentials']['token']
    self.nickname  = omni['user_info']['nickname']
  end

  def populate_from_facebook(omni)
    self.token  = omni['credentials']['token']
    self.nickname  = omni['user_info']['nickname']
  end
end
