class Collaboration < ActiveRecord::Base
  attr_accessible :collaborator_id

  belongs_to :collaborator, :class_name => "User"
  belongs_to :dream

  validates :collaborator_id, :presence => true
  validates :dream_id, :presence => true
end
