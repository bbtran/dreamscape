class Dream < ActiveRecord::Base
  belongs_to :user
  has_many :tasks, :dependent => :destroy
  has_many :collaborations, :dependent => :destroy
  has_many :collaborators, :through => :collaborations, :source => :collaborator

  accepts_nested_attributes_for :tasks
  validates :user_id, :presence => true
  validates :title,   :presence => true

  def add_collaborator!(collaborator)
    collaborations.create!(:collaborator_id => collaborator.id)
  end
end
