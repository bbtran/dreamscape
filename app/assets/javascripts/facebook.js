window.fbAsyncInit = function() {
    FB.init({appId: "170924039661923", status: true, cookie: true,
        xfbml: true});

    FB.getLoginStatus(function(o) {
        if (!o && o.status) return;
        if (o.status == 'connected') {
            // USER IS LOGGED IN AND HAS AUTHORIZED APP
            $("#main").removeClass('hidden');
        } else if (o.status == 'notConnected') {
            // USER IS LOGGED IN TO FACEBOOK (BUT HASN'T AUTHORIZED YOUR APP YET)
        } else {
            // // USER NOT CURRENTLY LOGGED IN TO FACEBOOK
            $.ajax({
                    url: '/users/sign_out',
                    type: 'DELETE',
                    success: function(html) {
                        window.location.reload();
                    }
                }
            );
        }
    });
};
(function() {
    var e = document.createElement('script');
    e.async = true;
    e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
    document.getElementById('fb-root').appendChild(e);
}());