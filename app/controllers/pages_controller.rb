class PagesController < ApplicationController
  before_filter :init_facebook_oauth, :force_facebook_auth, :init_facebook_crap

  def index
    @dream = Dream.new
    @dream.tasks.build
    @dreams = current_user.dreams.all(:include => 'tasks')
    @c_dreams = Collaboration.find_all_by_collaborator_id(current_user.id).map { |c| c.dream }
    @task = Task.new
    @friends = current_user.friends
  end
end
