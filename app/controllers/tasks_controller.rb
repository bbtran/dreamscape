class TasksController < ApplicationController
  before_filter :init_facebook_oauth, :force_facebook_auth, :init_facebook_crap
  before_filter :set_task, :only => [:edit, :update]
  before_filter :check_collaborators, :only => [:edit, :update]

  def create
    dream_id = Integer(params[:task][:dream_id])
    dream = Dream.find(dream_id)
    
    collabs = dream.collaborators
    redirect_to root_path unless dream.user == current_user || collabs.include?(current_user)

    @task = dream.tasks.build(params[:task])
    @task.user = current_user
    if @task.save
      redirect_to root_path, :flash => { :success => "Task created!" }
    else
      @dream = Dream.new
      @dreams = current_user.dreams
      render 'pages/index'
    end
  end

  def edit
    @friends = current_user.friends.map { |fr| [fr.name, fr.id] }
    @friends.unshift([current_user.name, current_user.id])
  end

  def update
    if @task.update_attributes(params[:task])
      assignee_ids = params[:assignee_ids]
      assignees = @task.assignees
      unless assignee_ids.nil?
        assignee_ids.each do |id|
          @task.assignments.create!({:assignee_id => id}) if assignees.find_by_id(id).nil?
        end
      end
      flash[:success] = "Task updated."
      redirect_to '/pages/index'
    else
      render 'edit'
    end
  end

  def destroy
    @task = Task.find_by_id(params[:id])
    @task.destroy
    redirect_to root_path, :flash => {:success => "Task deleted!"}
  end

  private

  def set_task
    @task = Task.find_by_id(params[:id])
  end

  def check_collaborators
    collabs = @task.dream.collaborators
    redirect_to root_path unless @task.dream.user == current_user || @task.user == current_user || collabs.include?(current_user)
  end

end
