require 'base64'

class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  attr_accessor :omniauth_data, :preexisting_authorization_token

  before_filter :init_facebook_oauth, :set_omniauth_data

  def method_missing(provider)
    return super unless valid_provider?(provider)
    omniauthorize_additional_account || omniauth_sign_in || omniauth_sign_up
  end

  def add_fb_friends(user)
    friends_data = @graph.get_connections("me", "friends")
    friend_uids = friends_data.map { |d| d['id'] }
    existing_uids = user.friends.map do |f|
      fb_token = f.user_tokens.find_by_provider('facebook')
      fb_token.uid
    end
    friend_uids.delete_if { |f| existing_uids.include?(f) }
    friend_tokens = UserToken.find_all_by_uid(friend_uids, :include => :user)
    friends = friend_tokens.map { |f| f.user }
    user.add_friends!(friends)
  end
  
  def omniauth_sign_in
    #todo merge by email if signing in with a new account for which we already have a user (match on email)
    return false unless preexisting_authorization_token
    flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => omniauth_data['provider']
    user = preexisting_authorization_token.user
    add_fb_friends(user)
    sign_in_and_redirect(:user, user)
    true
  end

  def omniauth_sign_up
    unless omniauth_data.recursive_find_by_key("email").blank?
      user = User.find_or_initialize_by_email(:email => omniauth_data.recursive_find_by_key("email"))
    else
      user = User.new
    end
    user.password = Devise.friendly_token[0,20]

    if user.save
      add_fb_friends(user)
      user.apply_omniauth(omniauth_data)
      user.save
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => omniauth_data['provider']
      sign_in_and_redirect(:user, user)
    else
      session[:omniauth] = omniauth_data.except('extra')
      redirect_to new_user_registration_url
    end
  end

  def omniauthorize_additional_account
    return false if current_user.nil?

    #todo signin not necessary, may mess up last sign in dates
    if preexisting_authorization_token && preexisting_authorization_token != current_user
      flash[:alert] = "You have created two accounts and they can't be merged automatically. Email #{LIVE_PERSONS_EMAIL} for help."
      sign_in_and_redirect(:user, current_user)
    else

      current_user.apply_omniauth(omniauth_data)
      current_user.save

      flash[:notice] = "Account connected"
      sign_in_and_redirect(:user, current_user)
    end
  end

  private

  def set_omniauth_data
    self.omniauth_data = env["omniauth.auth"]
    unless current_user.nil?
      init_facebook_graph(current_user.user_tokens.find_by_provider('facebook').token)
      sign_in_and_redirect(:user, current_user)
      true
    else
      unless omniauth_data.nil?
        init_facebook_graph(omniauth_data['credentials']['token'])
        self.preexisting_authorization_token = UserToken.find_by_provider_and_uid(omniauth_data['provider'], omniauth_data['uid'])
      end
    end
  end

  def valid_provider?(provider)
    !User.omniauth_providers.index(provider).nil?
  end
end