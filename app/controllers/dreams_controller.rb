class DreamsController < ApplicationController
  before_filter :init_facebook_oauth, :force_facebook_auth, :init_facebook_crap

  def create
    @dream = current_user.dreams.build(params[:dream])
    if @dream.save
      redirect_to root_path, :flash => { :success => "Dream created!" }
    else
      @dreams = current_user.dreams.all(:include => 'tasks')
      @task = Task.new
      render 'pages/index'
    end
  end

  def edit
    @dream = current_user.dreams.find(params[:id])
  end

  def update
    @dream = current_user.dreams.find(params[:id])
    if @dream.update_attributes(params[:dream])
      flash[:success] = "Dream updated."
      redirect_to '/pages/index'
    else
      render 'edit'
    end
  end

  def destroy
    @dream = current_user.dreams.find_by_id(params[:id])
    @dream.destroy
    redirect_to root_path, :flash => {:success => "Dream deleted!"}
  end
end
