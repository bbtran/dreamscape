class CollaborationsController < ApplicationController
  before_filter :init_facebook_oauth, :force_facebook_auth, :init_facebook_crap

  def create
    @dream = Dream.find(params[:id])
    @dream.add_collaborator!(current_user)
    redirect_to root_path, :flash => { :success => "Collaborating on dream '#{@dream.title}'" }
  end

  def destroy
    @dream = Dream.find(params[:id])
    @dream.collaborations.find_by_collaborator_id(current_user.id).destroy
    redirect_to root_path, :flash => {:success => "Stopped collaborating on dream '#{@dream.title}'"}
  end
end
