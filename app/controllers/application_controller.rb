class ApplicationController < ActionController::Base
  include RedirectBack

  attr_accessor :oauth, :graph
  helper_method :resource_class
  #protect_from_forgery

  def current_user
    super || NilUser.new
  end

  def user_signed_in?
    !current_user.nil?
  end

  def force_facebook_auth
    redirect_to user_omniauth_authorize_path(:facebook) unless user_signed_in?
  end

  def init_facebook_oauth
   @oauth ||= Koala::Facebook::OAuth.new(FACEBOOK_APP_ID, FACEBOOK_APP_SECRET, FACEBOOK_CANVAS_URL)
  end
  
  def init_facebook_graph(access_token)
    @graph ||= Koala::Facebook::API.new(access_token)
  end
  
  def init_facebook_crap
    token = current_user.user_tokens.find_by_provider('facebook')
    init_facebook_graph(token.token)
  end
end
