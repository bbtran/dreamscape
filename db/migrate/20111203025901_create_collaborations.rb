class CreateCollaborations < ActiveRecord::Migration
  def change
    create_table :collaborations do |t|
      t.integer :dream_id
      t.integer :collaborator_id
      t.timestamps
    end
    add_index :collaborations, :dream_id
    add_index :collaborations, :collaborator_id
  end
end
