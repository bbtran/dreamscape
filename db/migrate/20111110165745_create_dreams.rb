class CreateDreams < ActiveRecord::Migration
  def change
    create_table :dreams do |t|
      t.integer :user_id
      t.string :title
      t.string :details
      t.boolean :completed, :default => false
      t.timestamps
    end
  end
end
