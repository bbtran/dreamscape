class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.integer :dream_id
      t.integer :user_id
      t.string :title
      t.string :details
      t.boolean :completed, :default => false

      t.timestamps
    end
  end
end
